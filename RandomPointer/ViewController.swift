//
//  ViewController.swift
//  RandomPointer
//
//  Created by Wai Tsang on 4/28/16.
//  Copyright © 2016 Wai Tsang. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    // button action
    @IBAction func tapHere(sender: UIButton) {
        print("click Tap Here")
        
        showArrowImgView.alpha = 1.0
        
        if generateRandomNumber() == 1 {
            showArrowImgView.image = UIImage(named: "arrowLeft")
        } else {
            showArrowImgView.image = UIImage(named: "arrowRight")
        }
        
        UIView.animateWithDuration(1, delay: 1,
                                   options: UIViewAnimationOptions.CurveEaseOut,
                                   animations: {
                                    
                                    self.showArrowImgView.alpha = 0.0
                                    
            }, completion: nil)

    }
    
    
    @IBOutlet weak var showArrowImgView: UIImageView!
    
    // generate a random number 0 or 1
    func generateRandomNumber() -> Int {
        return Int(arc4random_uniform(2))
    }
}
